#!/bin/bash

# print a list of instances
gcloud compute instances list # | awk '{ print $1 }' | grep -v NAME

# get user input on which compute istance to delete
printf '\n'
echo -n "Enter instace NAME to delete and press [ENTER]: "
read VM_NAME

echo 'Preparing to delete VM: ' $VM_NAME

# delete vm 
gcloud compute instances delete $VM_NAME
