#!/bin/bash

# Purpose:  Deploy a centos 7 snapshot with pre-configured ssh keys

#Bad arguments
if [ $? -ne 0 ];
then
  exit 1
fi

# variables 
ZONE='us-west2-a'
VM_NAME=$1
SNAPSHOT='clean-cent-01'

# create disk 
gcloud compute disks create $VM_NAME --zone $ZONE --source-snapshot $SNAPSHOT #clean-cent

# create instance, give it external ip address, then boot it up! 
gcloud compute instances create $VM_NAME --disk name=$VM_NAME,boot=yes --zone=$ZONE

# get the external ip address of the VM 
EXTERNAL_IP=$(gcloud compute instances list $VM_NAME | awk '{ print $5 }' | grep -v INTERNAL_IP)

# create static entry in /etc/hosts 
printf "adding static entry for instance: $EXTERNAL_IP $VM_NAME into /etc/hosts"
echo $EXTERNAL_IP $VM_NAME >> /etc/hosts
